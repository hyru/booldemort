from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from utils.constants import RESOURCES
from game.models import Game
import random

from game.exceptions import ActionExceptionError


class Player(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    player_turn = models.IntegerField()
    build_settlement_created = models.BooleanField(default=False)
    build_road_created = models.BooleanField(default=False)
    used_robber = models.BooleanField(default=False)
    colour = models.CharField(max_length=100)
    development_card_played = models.BooleanField(default=False)
    development_card_played_count = models.IntegerField(default=0)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    victory_points = models.PositiveIntegerField(default=0)
    hidden_vp = models.PositiveIntegerField(default=0)
    resources_last_gained = models.CharField(max_length=1000)

    class Meta:
        unique_together = ['player_turn', 'game']

    def get_game(self):
        return self.game

    def get_username(self):
        return str(self.user)

    def get_development_card_played_count(self):
        return self.development_card_played_count

    def can_build_road_in_vertices(self, vertex1, vertex2):
        if vertex1 == vertex2:
            raise ActionExceptionError("Repeated roads.")
        road = vertex1.get_roads(vertex2)

        if road is not None:
            return False
        can1 = vertex1.can_build_road_of_player(self)
        can2 = vertex2.can_build_road_of_player(self)

        # if vertex1.to_tuple() == (1, 13) and vertex2.to_tuple() == (1, 14):
        #     print(can1, can2)

        # if vertex1.to_tuple() == (1, 14) and vertex2.to_tuple() == (1, 13):
        #     print(can1, can2)

        return can1 or can2

    def have_resources_for_settlement(self):
        brick = self.get_resource('brick')
        lumber = self.get_resource('lumber')
        grain = self.get_resource('grain')
        wool = self.get_resource('wool')
        # print(brick.amount, lumber.amount, grain.amount, wool.amount)
        return brick.amount > 0 and lumber.amount > 0 and \
            grain.amount > 0 and wool.amount > 0

    def have_resources_for_road(self):
        brick = self.get_resource('brick')
        lumber = self.get_resource('lumber')
        # print(brick.amount, lumber.amount, grain.amount, wool.amount)
        return brick.amount > 0 and lumber.amount > 0

    def available_actions(self):
        """
        This method verifies all possible actions that the player can do
        :returns:
            - available_actions - list of json with available actions from
                player, for the endpoint GET game/:pk/player/actions
            - actions - list of string with available actions from player,
                to check that the action sent by post is available
        """

        if self.game.is_game_closed():
            return [], []
        # print(self.user.room_set.all())
        available_actions = []

        current_player_turn = self.game.get_player_turn()
        my_turn = self.player_turn
        turn = self.game.turn

        if current_player_turn != my_turn:
            return available_actions, []

        # build_settlement
        # TODO It can only be when there are roads
        payload = []
        vertices = self.game.vertex_set.all()
        for vertex in vertices:
            if vertex.is_used():
                continue
            can1 = True
            neighbors = vertex.get_neighbors()
            for neighbor in neighbors:
                if neighbor.is_used():
                    can1 = False
                    break

            roads = vertex.get_roads()
            can2 = False
            for road in roads:
                owner_username = road.owner.user.username
                player_username = self.get_username()
                if owner_username == player_username:
                    can2 = True
                    break
            if can1 and (can2 or self.game.turn < 2):
                payload.append({
                    "index": vertex.index,
                    "level": vertex.level
                })
        # print(payload)
        if payload:
            if turn < 2 and not self.build_settlement_created:
                available_actions.append({
                    "type": "build_settlement",
                    "payload": payload
                })
            elif turn >= 2 and self.have_resources_for_settlement():
                available_actions.append({
                    "type": "build_settlement",
                    "payload": payload
                })

        # upgrade_city
        payload = []
        for vertex in vertices:
            if vertex.used and not vertex.settlement.upgrade:
                payload.append({
                    "index": vertex.index,
                    "level": vertex.level
                })

        if payload and self.game.turn >= 2:
            available_actions.append({
                "type": "upgrade_city",
                "payload": payload
            })

        # move_robber and play_knight_card
        dices = self.game.get_full_dice()
        have_card_knight = self.card_set.filter(card_type="knight").exists()
        if not self.development_card_played and \
           ((dices == 7 or have_card_knight) and self.game.turn >= 2):

            payload = []
            game = self.game
            board = game.get_board()
            hexagons = board.hexagon_set.all()
            for hexagon in hexagons:
                index = hexagon.index
                level = hexagon.level
                vertices = game.get_vertex_from_hexagon(index, level)
                data = {
                    "position": {
                        "index": index,
                        "level": level
                    },
                    "players": []
                }
                for vertex in vertices:
                    is_used = vertex.is_used()
                    if is_used and vertex.settlement.owner != self:
                        player = vertex.settlement.owner.user.username
                        if player not in data["players"]:
                            data["players"].append(player)
                payload.append(data)

            if self.game.get_full_dice() == 7 and not self.used_robber:
                available_actions.append({
                    "type": "move_robber",
                    "payload": payload
                })

            if self.card_set.filter(card_type="knight").exists():
                available_actions.append({
                    "type": "play_knight_card",
                    "payload": payload
                })

        # buy_card
        wool = self.get_resource('wool').amount
        grain = self.get_resource('grain').amount
        ore = self.get_resource('ore').amount

        if wool > 0 and grain > 0 and ore > 0:
            available_actions.append({
                "type": "buy_card",
                "payload": None
            })

        # end_turn
        # TODO if dices is 7 no can end turn
        can_pass = self.build_settlement_created and self.build_road_created
        if turn < 2 and can_pass:
            available_actions.append({
                "type": "end_turn",
                "payload": None
            })
        elif turn >= 2:
            if self.game.get_full_dice() == 7 and self.used_robber:
                available_actions.append({
                    "type": "end_turn",
                    "payload": None
                })
            elif self.game.get_full_dice() != 7:
                available_actions.append({
                    "type": "end_turn",
                    "payload": None
                })

        # bank_trade
        resource = self.resource_set.filter(amount__gte=4)

        if resource.exists():
            available_actions.append({
                "type": "bank_trade",
                "payload": None
            })

        # TODO build_road (importante para los test @mateo)
        payload = []
        brick = self.get_resource('brick').amount
        lumber = self.get_resource('lumber').amount
        if (lumber > 0 and brick > 0) or \
           (self.build_settlement_created and turn < 2):
            vertices = self.game.vertex_set.all()
            for vertex in vertices:
                for adjacent_vertex in vertex.get_neighbors():

                    can_build = self.can_build_road_in_vertices(
                        vertex,
                        adjacent_vertex
                    )

                    if can_build:
                        vertex1 = {
                            "index": vertex.index,
                            "level": vertex.level
                        }
                        vertex2 = {
                            "index": adjacent_vertex.index,
                            "level": adjacent_vertex.level
                        }
                        if [vertex2, vertex1] not in payload:
                            payload.append([vertex1, vertex2])

            if payload:
                if self.game.turn < 2 and not self.build_road_created:
                    available_actions.append({
                        "type": "build_road",
                        "payload": payload
                    })
                elif self.game.turn >= 2 and self.have_resources_for_road():
                    available_actions.append({
                        "type": "build_road",
                        "payload": payload
                    })

        # TODO play_road_building_card
        payload = []
        if self.card_set.filter(card_type="road_building").exists():
            vertices = self.game.vertex_set.all()
            for vertex in vertices:
                for adjacent_vertex in vertex.get_neighbors():
                    can_build = self.can_build_road_in_vertices(
                        vertex,
                        adjacent_vertex
                    )
                    if can_build:
                        vertex1 = {
                            "index": vertex.index,
                            "level": vertex.level
                        }
                        vertex2 = {
                            "index": adjacent_vertex.index,
                            "level": adjacent_vertex.level
                        }
                        if [vertex2, vertex1] not in payload:
                            payload.append([vertex1, vertex2])
            if payload:
                available_actions.append({
                    "type": "play_road_building_card",
                    "payload": payload
                })
        # TODO play_monopoly_card
        # TODO play_year_of_plenty_card

        actions = set([action["type"] for action in available_actions])
        # print(available_actions, end="\n\n\n")
        return available_actions, actions

    def get_cities(self):
        return self.settlement_set.all()

    def get_roads(self):
        return self.road_set.all()

    def get_total_resources(self):
        resources = self.resource_set.all()
        return sum(map(lambda resource: resource.amount, resources))

    def get_resource(self, res):
        return self.resource_set.get(resource=res)

    def get_resource_amount(self, res):
        resource = self.resource_set.get(resource=res)
        return resource.amount

    @staticmethod
    def create_resources(sender, **kwargs):
        job = kwargs.get('instance')
        if kwargs['created']:
            for r in RESOURCES:
                if r[0] == 'desert':
                    continue
                job.resource_set.create(resource=r[0])

    def set_resources(self, resources):
        for resource in resources:
            r = self.get_resource(resource[0])
            r.set(resource[1])
            r.save()

    def increase_resources(self, resources):
        resource_list = []
        add_resource = self.resources_last_gained
        for resource in resources:
            r = self.get_resource(resource[0])
            r.add(resource[1])
            resource_list.append(r)
            for i in range(0, resource[1]):
                add_resource = add_resource + '/' + resource[0]
        for resource in resource_list:
            resource.save()
        self.resources_last_gained = add_resource
        self.save()

    def decrease_resources(self, resources):
        resource_list = []
        for resource in resources:
            r = self.resource_set.get(resource=resource[0])
            r.decrement(resource[1])
            resource_list.append(r)
        for resource in resource_list:
            resource.save()

    def decrement_random_resource(self):
        total_resources = self.get_total_resources()
        if total_resources <= 0:
            raise ActionExceptionError("Not enough resources.")
        resources = self.resource_set.filter(amount__gt=0)
        resource = random.choice(resources)
        resource.decrement()
        resource.save()
        return resource

    def remove_random_resources(self, amount):
        while amount > 0:
            self.decrement_random_resource()
            amount -= 1

    # Actions methods
    def move_robber(self, data, knight_card=False):
        game = self.game
        player_to_steal = data.get('player', None)
        position = data.get('position', None)
        index = position['index']
        level = position['level']
        hexagon = game.get_hexagon(index, level)
        game.robber = hexagon

        self.used_robber = True
        if player_to_steal is not None:
            stolen_player = game.get_player_from_username(player_to_steal)
            hexagon = game.get_hexagon(index, level)
            vertices = game.get_vertex_from_hexagon(index, level)

            for vertex in vertices:
                settlement = vertex.get_settlement()
                if settlement and settlement.owner == stolen_player:
                    stolen_resource = stolen_player.decrement_random_resource()
                    stolen_resource.save()
                    resource = self.get_resource(stolen_resource.resource)
                    resource.add(1)
                    resource.save()
                    game.save()
                    self.development_card_played = True
                    self.save()
                    return "robber positioned and Player stolen.", 200
            raise ActionExceptionError("Player not in hexagon.")
        game.save()
        self.development_card_played = True
        self.save()
        return "robber positioned.", 200

    def play_knight_card(self, data):
        instance = self.card_set.get(card_type="knight")

        response = self.move_robber(data, knight_card=True)
        instance.delete()
        cards_count = self.development_card_played_count - 1
        self.development_card_played_count = cards_count
        self.save()
        return response

    def set_vp(self, amount):
        self.victory_points = amount

    def set_hvp(self, amount):
        self.hidden_vp = amount

    def increase_vp(self, amount):
        self.victory_points += amount

    def increase_hvp(self, amount):
        self.hidden_vp += amount

    def build_settlement(self, data):
        # TODO check neighbours
        game = self.game
        limit = [6, 18, 30]
        level = data['level']
        index = data['index']
        if not (0 <= level < 3 and 0 <= index < limit[level]):
            raise ActionExceptionError("Index or level out of bounds.")

        vertex = game.vertex_set.get(**data)
        if vertex.used:
            raise ActionExceptionError("Vertex alredy in use.")

        if game.turn >= 2:
            needed_resources = [('brick', 1), ('lumber', 1),
                                ('grain', 1), ('wool', 1)]
            self.decrease_resources(needed_resources)
        self.settlement_set.create(vertex=vertex)
        self.build_settlement_created = True
        vertex.used = True
        vertex.save()
        self.increase_vp(1)
        self.save()
        self.check_if_win()
        return "Created settlement.", 201

    def check_valid_road(self, v1, v2):
        game = self.game
        limit = [6, 18, 30]
        if not (0 <= v1['level'] < 3 and 0 <= v2['level'] < 3):
            raise ActionExceptionError("Level out of bounds")
        if not (0 <= v1['index'] < limit[v1['level']]
                and 0 <= v2['index'] < limit[v2['level']]):
            raise ActionExceptionError("Index out of bounds.")
        vertex1 = game.vertex_set.get(**v1)
        vertex2 = game.vertex_set.get(**v2)
        if not self.can_build_road_in_vertices(vertex1, vertex2):
            error = ("No adjacent construction, "
                     "edge alredy in use or invalid vertices.")
            raise ActionExceptionError(error)

    def create_road(self, vertex1, vertex2):
        road = self.road_set.create(v1=vertex1, v2=vertex2)
        road.save()

    def purchase_road(self):
        if self.game.turn >= 2:
            needed_resources = [('brick', 1), ('lumber', 1)]
            self.decrease_resources(needed_resources)

    def build_road(self, data):
        game = self.game
        if len(data) < 2:
            raise ActionExceptionError("Insufficient arguments")
        elif len(data) > 2:
            raise ActionExceptionError("Too many arguments")
        v1 = data[0]
        v2 = data[1]
        self.check_valid_road(v1, v2)
        vertex1 = game.vertex_set.get(**v1)
        vertex2 = game.vertex_set.get(**v2)
        self.purchase_road()
        self.create_road(vertex1, vertex2)
        self.build_road_created = True
        self.save()
        return "Created road.", 201

    def bank_trade(self, data):
        give = data['give']
        receive = data['receive']
        resources = [res[0] for res in RESOURCES]

        if give == receive:
            raise ActionExceptionError("Resources must be different.")
        elif give not in resources or receive not in resources:
            raise ActionExceptionError("Resource not exists.")

        resource = self.resource_set.get(resource=give)

        if resource.amount < 4:
            raise ActionExceptionError("Insufficient resources.")

        new_resource = self.resource_set.get(resource=receive)
        new_resource.add(1)
        resource.decrement(4)

        resource.save()
        new_resource.save()
        return "Trade done.", 200

    def give_card(self, card):
        cards_types = ['road_building', 'year_of_plenty',
                       'monopoly', 'victory_point', 'knight']
        if card not in cards_types:
            return
        self.card_set.create(card_type=card)
        if card == "victory_point":
            self.increase_hvp(1)
            self.save()
            self.check_if_win()

    def buy_card(self, data):
        cards_types = ['road_building', 'year_of_plenty',
                       'monopoly', 'victory_point', 'knight']
        needed_resources = [('wool', 1), ('grain', 1),  ('ore', 1)]
        self.decrease_resources(needed_resources)
        card = random.randrange(0, len(cards_types))
        self.card_set.create(card_type=cards_types[card])
        if cards_types[card] == "victory_point":
            self.increase_hvp(1)
            self.save()
            self.check_if_win()
        count_buy_cards = self.development_card_played_count + 1
        self.development_card_played_count = count_buy_cards
        self.save()
        return "Card purchased", 201

    def check_if_win(self):
        game = self.game
        if ((self.victory_points + self.hidden_vp) == 10):
            game.declare_winner(self)

    def end_turn(self, data):
        self.game.end_turn()
        self.refresh_from_db()
        self.build_settlement_created = False
        self.build_road_created = False
        self.development_card_played = False
        self.used_robber = False
        self.save()

        return "turn passed ok", 201

    def play_road_building_card(self, data):
        game = self.game
        cards = self.card_set.filter(card_type='road_building')
        if cards.count() == 0:
            raise ActionExceptionError("Road building card missing")
        card = cards.first()
        if len(data) < 2:
            raise ActionExceptionError("Insufficient arguments")
        elif len(data) > 2:
            raise ActionExceptionError("Too many arguments")
        for road_data in data:
            if len(road_data) < 2:
                raise ActionExceptionError("Insufficient arguments")
            elif len(road_data) > 2:
                raise ActionExceptionError("Too many arguments")
        for road_data in data:
            v1 = road_data[0]
            v2 = road_data[1]
            self.check_valid_road(v1, v2)
        if (((data[0][0] == data[1][0]) and (data[0][1] == data[1][1])) or
                (data[0][0] == data[1][1]) and (data[0][1] == data[1][0])):
            raise ActionExceptionError("Repeated roads")
        for road_data in data:
            v1 = road_data[0]
            v2 = road_data[1]
            vertex1 = game.vertex_set.get(**v1)
            vertex2 = game.vertex_set.get(**v2)
            self.create_road(vertex1, vertex2)
        card.delete()
        cards_count = self.development_card_played_count - 1
        self.development_card_played_count = cards_count
        self.save()
        return "Created road.", 201

    def __str__(self):
        return str(self.user.username)


post_save.connect(Player.create_resources, sender=Player)
