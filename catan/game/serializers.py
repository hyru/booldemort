from rest_framework import serializers

from board.serializers import HexagonPositionSerializer

from settlement.serializers import SettlementSerializer

from .models import Game, Vertex
from player.models import Player


class GameSerializer(serializers.ModelSerializer):
    resources = serializers.StringRelatedField(many=True)
    cards = serializers.StringRelatedField(many=True)

    class Meta:
        model = Game
        fields = ('cards', 'resources', 'player_turn', 'dice1', 'dice2')


class VertexSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vertex
        fields = ('level', 'index')


class PlayerStatusSerializer(serializers.ModelSerializer):
    """
    output:
    "PLAYER = {
        username: USERNAME,
        colour: string,
        settlements: [VERTEX_POSITION],
        cities: [VERTEX_POSITION],
        roads: [ROAD_POSITION],
        development_cards: integer,
        resources_cards: integer,
        victory_points: integer,
        last_gained: [RESOURCE]
    }"
    """

    username = serializers.SerializerMethodField()
    cities = serializers.SerializerMethodField()
    settlements = serializers.SerializerMethodField()
    roads = serializers.SerializerMethodField()
    resources_cards = serializers.SerializerMethodField()
    development_cards = serializers.SerializerMethodField()
    last_gained = serializers.SerializerMethodField()

    def get_username(self, instance):
        return instance.get_username()

    def get_settlements(self, instance):
        settlements = instance.settlement_set.filter(upgrade=False)
        return [SettlementSerializer(sett).data for sett in settlements]

    def get_cities(self, instance):
        settlements = instance.settlement_set.filter(upgrade=True)
        return [SettlementSerializer(sett).data for sett in settlements]

    def get_roads(self, instance):
        roads = instance.road_set.all()
        ret = []
        for road in roads:
            ret.append([
                {'index': road.v1.index, 'level': road.v1.level},
                {'index': road.v2.index, 'level': road.v2.level}
            ])
        return ret

    def get_resources_cards(self, instance):
        count = 0
        for resource in instance.resource_set.all():
            count += resource.amount
        return count

    def get_development_cards(self, instance):
        return instance.development_card_played_count

    def get_last_gained(self, instance):
        resources = instance.resources_last_gained.split('/')[1:]
        return resources

    class Meta:
        model = Player
        fields = ('username',
                  'colour',
                  'settlements',
                  'cities',
                  'roads',
                  'victory_points',
                  'resources_cards',
                  'development_cards',
                  "last_gained")


class GameStatusSerializer(serializers.ModelSerializer):
    """
    Output:
    {
        players: [ PLAYER ],
        robber: HEX_POSITION,
        current_turn: {
            user: USERNAME,
            dice: [int, int]
        },
        winner: optional USERNAME,
    }
    """
    current_turn = serializers.SerializerMethodField()
    players = serializers.SerializerMethodField()
    robber = HexagonPositionSerializer()

    def get_players(self, instance):
        players = instance.player_set.all()
        return [PlayerStatusSerializer(player).data for player in players]

    def get_current_turn(self, instance):
        player = instance.player_set.get(player_turn=instance.player_turn)
        username = player.user.username
        data = {
            'user': username,
            'dice': [instance.dice1, instance.dice2]
        }
        return data

    class Meta:
        model = Game
        fields = ("players", "robber", "current_turn", "winner")
