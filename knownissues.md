# Los Colonos de Catan


### Known issues


* Lobby
    * Sometimes the game fails to start. Reported by front-end team, couldn't be replicated.


* Roads
    * Sometimes  roads on already used edges can be selected and built
    * Sometimes roads conecting two non adjacent vertexes can be selected and built

* Settlements:
    * Sometimes the game doesnt allow to build any settlement even tough it should. Could only be repiclated once.

