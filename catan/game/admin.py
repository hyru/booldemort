from django.contrib import admin
from .models import Game, Vertex


class GameAdmin(admin.ModelAdmin):
    list_display = ['id', 'dice1', 'dice2', 'player_turn', 'robber']


admin.site.register(Game, GameAdmin)
admin.site.register(Vertex)
