from django.db.models.signals import post_save
from django.db import models
import random
from utils.constants import vertices


class Game(models.Model):
    room = models.OneToOneField(
        "lobby.Room",
        on_delete=models.CASCADE
    )
    id = models.AutoField(primary_key=True, editable=False)
    player_turn = models.IntegerField(default=0)
    turn = models.IntegerField(default=0)
    winner = models.CharField(max_length=100, blank=True, null=True)
    dice1 = models.IntegerField(default=random.randint(1, 6))
    dice2 = models.IntegerField(default=random.randint(1, 6))
    robber = models.ForeignKey(
        'board.Hexagon',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    def save(self, *args, **kwargs):
        if self.robber is None:
            board = self.get_board()
            self.robber = board.get_desert()
        super(Game, self).save(*args, **kwargs)

    @staticmethod
    def create_vertex(sender, **kwargs):
        job = kwargs.get('instance')
        if kwargs['created']:
            for level in [6, 18, 30]:
                for index in range(level):
                    vertex_data = {
                        "game": job,
                        "index": index,
                    }
                    if level == 6:
                        vertex_data['level'] = 0
                    elif level == 18:
                        vertex_data['level'] = 1
                    else:
                        vertex_data['level'] = 2
                    job.vertex_set.create(**vertex_data)

    def get_full_dice(self):
        return self.dice1 + self.dice2

    def get_board(self):
        return self.room.board

    def get_hexagon(self, index, level):
        board = self.get_board()
        hexagon = board.hexagon_set.get(index=index, level=level)
        return hexagon

    def get_vertex(self, index, level):
        return self.vertex_set.get(index=index, level=level)

    def get_dices(self):
        return self.dice1, self.dice2

    def get_player_turn(self):
        return self.player_turn

    def get_player_from_username(self, username):
        return self.player_set.get(user__username=username)

    def throw_dice(self):
        self.dice1 = random.randint(1, 6)
        self.dice2 = random.randint(1, 6)
        self.save()

    def end_turn(self):
        self.throw_dice()
        if self.get_full_dice() == 7:
            players = self.player_set.all()
            for player in players:
                total = player.get_total_resources()
                if total > 7:
                    player.remove_random_resources(total//2)

        turn = self.player_turn
        number_of_players = self.player_set.count()
        self.player_turn = (turn + 1) % number_of_players
        if turn == number_of_players - 1:
            for player in self.player_set.all():
                player.resources_last_gained = ""
                player.save()
            self.turn += 1
        self.save()
        self.distribute_resources()

    def is_game_closed(self):
        return self.winner is not None

    def declare_winner(self, player):
        username = player.get_username()
        self.winner = username
        self.save()

    def distribute_resources(self):
        dice = self.dice1 + self.dice2
        hexagons = self.room.board.hexagon_set.all()
        if self.turn < 2:
            return
        elif self.turn == 2 and self.player_turn > 0:
            return
        if self.turn > 2:
            hexagons = self.room.board.hexagon_set.filter(token=dice)

        for hexagon in hexagons:
            if str(hexagon.resource) == 'desert':
                continue
            ver = hexagon.get_neighboring_vertices()
            for v in ver:
                level = v['level']
                index = v['index']
                vertex = self.vertex_set.get(level=level, index=index)
                settl = None
                try:
                    settl = vertex.settlement
                except Exception:
                    continue
                if settl is not None:
                    player = settl.owner
                    resource = hexagon.resource
                    player.increase_resources([(resource, 1)])

    def get_vertex_from_hexagon(self, index, level):
        hexagon = self.get_hexagon(index, level)
        hexagon_vertex = hexagon.get_neighboring_vertices()
        ret = list(map(
            lambda vertex: self.get_vertex(
                index=vertex['index'],
                level=vertex['level']
            ),
            hexagon_vertex
        )
        )
        return ret

    def __str__(self):
        return str(self.id)


post_save.connect(Game.create_vertex, sender=Game)


class Vertex(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    level = models.PositiveIntegerField(default=0)
    index = models.PositiveIntegerField(default=0)
    # TODO delete this field
    used = models.BooleanField(default=False)

    class Meta:
        unique_together = ['game', 'level', 'index']

    def get_settlement(self):
        try:
            return self.settlement
        except Exception:
            return None

    def to_tuple(self):
        return (self.level, self.index)

    def is_used(self):
        return self.get_settlement() is not None

    def get_roads(self, vertex=None):
        if vertex is None:
            road_in = self.road_in.all()
            road_out = self.road_out.all()
            return road_in.union(road_out)
        try:
            roads1 = self.get_roads()
            roads2 = vertex.get_roads()
            intersection = roads1.intersection(roads2)
            # if vertex.to_tuple() == (1, 13) or vertex.to_tuple() == (1, 14):
            #     print(union)
            #     print(self, ": ", roads1)
            #     print(vertex, ": ", roads2)
            #     print(intersection)
            #     print("--------")
            #     print(self.to_tuple(), " ", vertex.to_tuple())
            #     print("DEBUG")
            #     print(ret)
            return intersection[0]
        except Exception:
            return None

    def can_build_road_of_player(self, player):
        roads = self.get_roads()
        for road in roads:
            if road.owner == player:
                return True
        settlement = self.get_settlement()
        return settlement and settlement.owner == player

    def get_neighbors(self):
        game = self.game
        neighbors = vertices[(self.level, self.index)]
        ret = []
        for vertex in neighbors:
            data = {
                'index': vertex[1],
                'level': vertex[0]
            }
            neighbor = game.vertex_set.get(**data)
            ret.append(neighbor)
        return ret

    def __str__(self):
        return "({0}, {1})".format(self.level, self.index)
        # return "game {0} - id {1} - ({2}, {3})".format(
        #     self.game,
        #     self.id,
        #     self.level,
        #     self.index
        # )
