# Colonos de Catan 

[![pipeline status](https://gitlab.com/hyru/booldemort/badges/master/pipeline.svg)](https://gitlab.com/hyru/booldemort/commits/master)
[![coverage report](https://gitlab.com/hyru/booldemort/badges/master/coverage.svg)](https://gitlab.com/hyru/booldemort/commits/master)

Catan is a multiplayer game created with Django Rest Framework.

## Install
### Requirements
* Python 3.7

``` bash
$ pip install -r requirements.txt
```

## Usage

Execute in /catan/
``` bash
$ ./migrate.sh
$ python manage.py runserver
```

## Testing

Execute in /catan/
```
$ python manage.py test
```
## Authors

* **Leandro Acosta** - [leandro.acosta](https://gitlab.com/leandro.acosta)
* **Lucas Caballero** - [rojomaldit](https://gitlab.com/rojomaldit)
* **Mateo Ostorero** - [hyru](https://gitlab.com/hyru)

## License
[MIT](https://choosealicense.com/licenses/mit/)
