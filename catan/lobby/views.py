from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import Room
from board.models import Board, Hexagon
from game.models import Game
from .serializers import RoomSerializer

from .exeptions import (
    RoomAlreadyExist,
    RoomNotExist,
    NameAlreadyExist,
    BoardNotExist,
)


class RoomsView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Room.objects.all()

    def create(self, request):
        try:
            name = request.data['name']
            board_id = request.data['board_id']
            if Room.objects.filter(name=name).exists():
                raise NameAlreadyExist
            if not Board.objects.filter(id=board_id).exists():
                raise BoardNotExist

            board = Board.objects.get(id=board_id)
            Room.objects.create(
                board=board,
                name=name,
                owner=request.user,
            )
        except RoomAlreadyExist:
            return Response(
                'The room already exists',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        except NameAlreadyExist:
            return Response(
                'Name already in use',
                status=status.HTTP_409_CONFLICT
            )
        except BoardNotExist:
            return Response(
                'The Board not exist',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        except Exception:
            return Response(
                'BADREQUEST',
                status=status.HTTP_400_BAD_REQUEST
            )

        return Response(status=status.HTTP_201_CREATED)

    def list_rooms(self, request):
        query_set = Room.objects.all()
        rooms = RoomSerializer(query_set, many=True).data
        return Response(rooms)

    def list_room(self, request, pk=None):
        if not Room.objects.filter(id=pk).exists():
            return Response(
                'The ROOM does not exist',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        query_set = self.get_object()
        room = RoomSerializer(query_set).data
        return Response(room)

    def join(self, request, pk=None):
        try:
            if not Room.objects.filter(id=pk).exists():
                raise RoomNotExist

            room = self.get_object()
            user = request.user

            if user in room.players.all():
                return Response('Already in the ROOM')
            if room.number_of_players() >= room.max_players:
                return Response('The ROOM is full')

        except RoomNotExist:
            return Response(
                'The ROOM does not exist',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        except Exception:
            return Response(
                'BADREQUEST',
                status=status.HTTP_400_BAD_REQUEST
            )
        room.players.add(user)
        return Response()

    def start_game(self, request, pk=None):
        if not Room.objects.filter(id=pk).exists():
            return Response(
                'The ROOM does not exist',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        room = self.get_object()

        if room.game_has_started:
            return Response(
                "The game has started",
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        if not (3 <= room.number_of_players() <= 4):
            return Response(
                "3 or 4 players are required",
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        colours = ['red', 'green', 'blue', 'yellow']
        hexagon = Hexagon.objects.filter(board=room.board)
        if not hexagon.exists():
            hexagon = []
            hexagon.append(Hexagon.objects.create(board=room.board))
            hexagon[0].save()

        game = Game.objects.create(
            room=room,
            robber=hexagon[0],
            id=room.id
        )

        for colour, user in enumerate(room.players.all()):
            game.player_set.create(
                user=user,
                colour=colours[colour],
                player_turn=colour
            )
        room.game_has_started = True
        room.save()

        return Response(
            status=status.HTTP_201_CREATED
        )

    def cancel_lobby(self, request, pk=None):
        if not Room.objects.filter(id=pk).exists():
            return Response(
                'The ROOM does not exist',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        room = self.get_object()
        if room.game_has_started:
            return Response(
                'The game already start',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        if request.user != room.owner:
            return Response(
                'The owner is the only than can delete this room',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

        room.delete()
        return Response()
