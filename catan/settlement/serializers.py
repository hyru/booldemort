from rest_framework import serializers

from .models import Settlement


class SettlementSerializer(serializers.ModelSerializer):

    level = serializers.SerializerMethodField()
    index = serializers.SerializerMethodField()

    def get_index(self, instance):
        return instance.vertex.index

    def get_level(self, instance):
        return instance.vertex.level

    class Meta:
        model = Settlement
        fields = ('level', 'index')
