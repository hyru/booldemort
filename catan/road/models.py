from django.db import models


class Road(models.Model):
    owner = models.ForeignKey('player.Player', on_delete=models.CASCADE)
    v1 = models.ForeignKey(
        'game.Vertex', on_delete=models.CASCADE, related_name='road_in')
    v2 = models.ForeignKey(
        'game.Vertex', on_delete=models.CASCADE, related_name='road_out')

    def __str__(self):
        vertex1 = "(" + str(self.v1.level) + ", " + str(self.v1.index) + ")"
        vertex2 = "(" + str(self.v2.level) + ", " + str(self.v2.index) + ")"
        return vertex1 + " - " + vertex2 + " from " + str(self.owner)

    class Meta:
        unique_together = ['owner', 'v1', 'v2']
